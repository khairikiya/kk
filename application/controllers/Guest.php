<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Guest extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Md_users');
    }
    public function index()
    {
        $data['title'] = "Layanan Konseling Kelola Kisah";
        $this->load->view('landing', $data);
    }
}
