<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Spadmin extends CI_Controller
{
    function __construct()
    {

        parent::__construct();
        $this->load->model('Md_users');
        $this->load->model('Md_slideshow');
        $this->load->model('Md_webinar');

        $this->load->library('image_lib');
        $this->load->library('upload');

        if (empty($this->session->userdata('is_login'))) {
            redirect('keki');
        }
    }

    public function index()
    {
        $data['title'] = "Keki Admin Dashboard";
        $this->load->view('spadmin/dashboard', $data);
    }

    public function manage_user($param1 = '', $param2 = '', $param3 = '')
    {
        if ($param1 == 'add') {
            $this->form_validation->set_rules('username', 'username', 'trim|required|min_length[1]|max_length[255]|is_unique[users.username]');
            $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[1]|max_length[255]');
            $this->form_validation->set_rules('nama', 'nama', 'trim|required|min_length[1]|max_length[255]');
            if ($this->form_validation->run() == true) {
                $username = $this->input->post('username');
                $password = $this->input->post('password');
                $nama = $this->input->post('nama');
                $data_user = array(
                    'username' => $username,
                    'password' => password_hash($password, PASSWORD_DEFAULT),
                    'nama' => $nama
                );
                $this->Md_users->register($data_user);
                $this->session->set_flashdata('success', 'Data berhasil ditambahkan');
                redirect('spadmin/manage_user');
            } else {
                $this->session->set_flashdata('error', validation_errors());
                redirect('spadmin/manage_user');
            }
        } else if ($param1 == 'edit') {

            $data['users'] = $this->Md_users->getUserById($param2);
            $data['title'] = "Edit User";
            $this->load->view('spadmin/edit_user', $data);
        } else if ($param1 == 'update') {

            $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[1]|max_length[255]');
            $this->form_validation->set_rules('nama', 'nama', 'trim|required|min_length[1]|max_length[255]');
            if ($this->form_validation->run() == true) {

                $password = $this->input->post('password');
                $nama = $this->input->post('nama');
                $data_user = array(
                    'password' => password_hash($password, PASSWORD_DEFAULT),
                    'nama' => $nama
                );
                $this->Md_users->updateUsers($param2, $data_user);
                $this->session->set_flashdata('success', 'Data berhasil diupdate');
                redirect('spadmin/manage_user/edit/' . $param2);
            } else {
                $this->session->set_flashdata('error', validation_errors());
                redirect('spadmin/manage_user/edit/' . $param2);
            }
        } else {

            $data['title'] = "List User";
            $data['users'] = $this->Md_users->getDataUsers();
            $this->load->view('spadmin/manage_user', $data);
        }
    }

    function manage_slideshow($param1 =  '', $param2 = '', $param3 = '')
    {

        if ($param1 == 'add') {

            $judul = $this->input->post('judul_slideshow');
            $deskripsi = $this->input->post('deskripsi_slideshow');
            $urutan = $this->input->post('urutan');

            if ($_FILES['media']['size'] > 0) {
                $file = $_FILES["media"]['tmp_name'];
                list($width, $height) = getimagesize($file);
                if ($width == 1920 && $height == 850) {
                    $config['upload_path'] = './assets/media/slideshow';
                    $config['allowed_types'] = 'gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG';
                    $config['max_size'] = '10000';
                    //  $config['max_width']       = '900';
                    //  $config['max_height']       = '467';
                    $config['file_name'] = str_replace(" ", "_", preg_replace('/[^a-zA-Z0-9\s+\']/', '', trim($judul))) . date('Ymdhis');

                    $this->upload->initialize($config);
                    ini_set('memory_limit', '-1');

                    if ($this->upload->do_upload('media')) {
                        $data = $this->upload->data();
                        $filename = $data['file_name'];
                        $file_ext = $data['file_ext'];
                        if (
                            $file_ext == '.gif' or $file_ext == '.jpg' or $file_ext == '.png' or $file_ext == '.jpeg' or $file_ext == '.GIF'
                            or $file_ext == '.JPG' or $file_ext == '.PNG' or $file_ext == '.JPEG'
                        ) {
                            //upload gambar
                            $dataSlideshow = array(
                                'judul_slideshow' => $judul,
                                'deskripsi_slideshow' => $deskripsi,
                                'img_slideshow' => $filename,
                                'urutan' => $urutan,
                                'status' => 1,
                            );
                            $insertMedia = $this->Md_slideshow->addSlide($dataSlideshow);

                            $this->session->set_flashdata('success', 'Data slideshow berhasil ditambahkan');
                            redirect('spadmin/manage_slideshow');
                        }
                    }
                } else {
                    $this->session->set_flashdata('error', 'Ukuran gambar harus 1920 x 850 pixel');
                    redirect('spadmin/manage_slideshow');
                }
            } else {
                $this->session->set_flashdata('error', 'Gambar tidak boleh kosong');
                redirect('spadmin/manage_slideshow');
            }
        } else if ($param1 == 'delete') {
            $this->Md_slideshow->updateSlideshow($param2, array('status' => 2));
            $this->session->set_flashdata('success', 'Slideshow berhasil dihapus');
            redirect('spadmin/manage_slideshow');
        } else {

            $data['title'] = "Manage Slideshow";
            $data['slide'] = $this->Md_slideshow->getDataSlideshow();
            $this->load->view('spadmin/manage_slideshow', $data);
        }
    }

    function manage_webinar($param1 = '', $param2 = '', $param3 = '')
    {
        if ($param1 == 'tambah') {
            $data['title'] = "Menambah Webinar";
            $this->load->view('spadmin/add_webinar', $data);
        } else if ($param1 == 'add') {
            $this->form_validation->set_rules('judul_webinar', 'judul_webinar', 'trim|required|min_length[1]|max_length[255]|is_unique[users.username]');
            $this->form_validation->set_rules('isi_webinar', 'isi_webinar', 'trim|required|min_length[1]|max_length[255]');
            $this->form_validation->set_rules('tgl_webinar', 'tgl_webinar', 'trim|required|min_length[1]|max_length[255]');
            $this->form_validation->set_rules('nama_psikolog', 'tgl_webinar', 'trim|required|min_length[1]|max_length[255]');

            $judul_webinar = $this->input->post('judul_webinar');
            $isi_webinar = $this->input->post('isi_webinar');
            $tgl_webinar = $this->input->post('tgl_webinar');
            $nama_psikolog = $this->input->post('nama_psikolog');

            if ($this->form_validation->run() == true) {
                if ($_FILES['media']['size'] > 0) {
                    $file = $_FILES["media"]['tmp_name'];
                    list($width, $height) = getimagesize($file);
                    if ($width == 800 && $height == 400) {
                        $config['upload_path'] = './assets/media/webinar';
                        $config['allowed_types'] = 'gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG';
                        $config['max_size'] = '10000';
                        $config['file_name'] = str_replace(" ", "_", preg_replace('/[^a-zA-Z0-9\s+\']/', '', trim($judul_webinar))) . date('Ymdhis');

                        $this->upload->initialize($config);
                        ini_set('memory_limit', '-1');

                        if ($this->upload->do_upload('media')) {
                            $data = $this->upload->data();
                            $filename = $data['file_name'];
                            $file_ext = $data['file_ext'];
                            if (
                                $file_ext == '.gif' or $file_ext == '.jpg' or $file_ext == '.png' or $file_ext == '.jpeg' or $file_ext == '.GIF'
                                or $file_ext == '.JPG' or $file_ext == '.PNG' or $file_ext == '.JPEG'
                            ) {
                                $data_webinar = array(
                                    'judul_webinar' => $judul_webinar,
                                    'isi_webinar' => $isi_webinar,
                                    'tgl_webinar' => $tgl_webinar,
                                    'nama_psikolog' => $nama_psikolog,
                                    'img_webinar' => $filename,
                                    'user_id' => $this->session->userdata('user_id'),
                                    'status' => 1
                                );
                                $this->Md_webinar->addWebinar($data_webinar);
                                $this->session->set_flashdata('success', 'Data Webinar berhasil ditambahkan');
                                redirect('spadmin/manage_webinar');
                            }
                        }
                    } else {
                        $this->session->set_flashdata('error', 'Ukuran gambar harus 1920 x 850 pixel');
                        redirect('spadmin/manage_webinar/tambah');
                    }
                } else {
                    $this->session->set_flashdata('error', 'Gambar tidak boleh kosong');
                    redirect('spadmin/manage_webinar/tambah');
                }
            } else {
                $this->session->set_flashdata('error', validation_errors());
                redirect('spadmin/manage_webinar/tambah');
            }
        } else if ($param1 == 'edit') {

            $data['webinar'] = $this->Md_webinar->getDataWebinarById($param2);
            $data['title'] = "Edit Webinar Kelola Kisah";
            $this->load->view('spadmin/edit_webinar', $data);
        } else if ($param1 == 'update') {
            $this->form_validation->set_rules('judul_webinar', 'judul_webinar', 'trim|required|min_length[1]|max_length[255]|is_unique[users.username]');
            $this->form_validation->set_rules('isi_webinar', 'isi_webinar', 'trim|required|min_length[1]|max_length[255]');
            $this->form_validation->set_rules('tgl_webinar', 'tgl_webinar', 'trim|required|min_length[1]|max_length[255]');
            $this->form_validation->set_rules('nama_psikolog', 'tgl_webinar', 'trim|required|min_length[1]|max_length[255]');

            $judul_webinar = $this->input->post('judul_webinar');
            $isi_webinar = $this->input->post('isi_webinar');
            $tgl_webinar = $this->input->post('tgl_webinar');

            $nama_psikolog = $this->input->post('nama_psikolog');

            if ($this->form_validation->run() == true) {

                if ($_FILES['media']['size'] == 0) {
                    $data_webinar = array(
                        'judul_webinar' => $judul_webinar,
                        'isi_webinar' => $isi_webinar,
                        'tgl_webinar' => $tgl_webinar,
                        'nama_psikolog' => $nama_psikolog,
                        'user_id' => $this->session->userdata('user_id'),
                        'status' => 1
                    );
                    $this->Md_webinar->updateWebinar($param2, $data_webinar);
                    $this->session->set_flashdata('success', 'Data Webinar berhasil diubah');
                    redirect('spadmin/manage_webinar/edit/' . $param2);
                }

                $file = $_FILES["media"]['tmp_name'];
                list($width, $height) = getimagesize($file);
                if ($width == 800 && $height == 400) {
                    $config['upload_path'] = './assets/media/webinar';
                    $config['allowed_types'] = 'gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG';
                    $config['max_size'] = '10000';
                    $config['file_name'] = str_replace(" ", "_", preg_replace('/[^a-zA-Z0-9\s+\']/', '', trim($judul_webinar))) . date('Ymdhis');

                    $this->upload->initialize($config);
                    ini_set('memory_limit', '-1');

                    if ($this->upload->do_upload('media')) {
                        $data = $this->upload->data();
                        $filename = $data['file_name'];
                        $file_ext = $data['file_ext'];
                        if (
                            $file_ext == '.gif' or $file_ext == '.jpg' or $file_ext == '.png' or $file_ext == '.jpeg' or $file_ext == '.GIF'
                            or $file_ext == '.JPG' or $file_ext == '.PNG' or $file_ext == '.JPEG'
                        ) {
                            $data_webinar = array(
                                'judul_webinar' => $judul_webinar,
                                'isi_webinar' => $isi_webinar,
                                'tgl_webinar' => $tgl_webinar,
                                'nama_psikolog' => $nama_psikolog,
                                'img_webinar' => $filename,
                                'user_id' => $this->session->userdata('user_id'),
                                'status' => 1
                            );
                            $this->Md_webinar->addWebinar($data_webinar);
                            $this->session->set_flashdata('success', 'Data Webinar berhasil ditambahkan');
                            redirect('spadmin/manage_webinar');
                        }
                    }
                } else {
                    $this->session->set_flashdata('error', 'Ukuran gambar harus 1920 x 850 pixel');
                    redirect('spadmin/manage_webinar/edit/' . $param2);
                }
            } else {
                $this->session->set_flashdata('error', validation_errors());
                redirect('spadmin/manage_webinar/edit/' . $param2);
            }
        } else if ($param1 == 'delete') {
            $this->Md_webinar->updateWebinar($param2, array('status' => 2));
            $this->session->set_flashdata('success', 'Slideshow berhasil dihapus');
            redirect('spadmin/manage_webinar');
        } else {
            $data['webinar'] = $this->Md_webinar->getDataWebinar();
            $data['title'] = "List Info Webinar";
            $this->load->view('spadmin/manage_webinar', $data);
        }
    }
}
