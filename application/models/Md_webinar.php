<?php
class Md_webinar extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function addWebinar($data_webinar)
    {
        $this->db->insert('webinar', $data_webinar);
    }

    function getDataWebinar()
    {
        $sql = "SELECT * FROM webinar WHERE STATUS = 1";
        $data = $this->db->query($sql);

        return $data->result();
    }
    function getDataWebinarById($id)
    {
        $sql = "SELECT * FROM webinar WHERE webinar_id = $id";
        $data = $this->db->query($sql);

        return $data->row();
    }
    function updateWebinar($id, $data)
    {
        $this->db->where('webinar_id', $id);
        $this->db->update('webinar', $data);
    }
}
