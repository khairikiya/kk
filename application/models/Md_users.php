<?php
class Md_users extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function register($data_user)
    {

        $this->db->insert('users', $data_user);
    }

    function getDataUsers()
    {
        $sql = "SELECT * FROM USERS WHERE STATUS = 1";
        $data = $this->db->query($sql);

        return $data->result();
    }
    function login_user($username, $password)
    {
        $query = $this->db->get_where('users', array('username' => $username));
        if ($query->num_rows() > 0) {
            $data_user = $query->row();
            if (password_verify($password, $data_user->password)) {
                $this->session->set_userdata('username', $username);
                $this->session->set_userdata('nama', $data_user->nama);
                $this->session->set_userdata('is_login', TRUE);
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    function cek_login()
    {
        if (empty($this->session->userdata('is_login'))) {
            redirect('keki');
        }
    }

    function getUserById($id)
    {

        $sql = "SELECT * FROM users where user_id = $id";
        return $this->db->query($sql)->row();
    }
    function updateUsers($id, $data)
    {
        $this->db->where('user_id', $id);
        $this->db->update('users', $data);
    }
}
