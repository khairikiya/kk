<?php include("header-front.php"); ?>
<?php include("navigation-front.php"); ?>
<!-- Content
		============================================= -->
<div class="" id="section-home"></div>

<section id="slider" class="slider-element slider-parallax full-screen dark" style="background: url(<?= base_url(); ?>assets/media/slideshow/banner.png) center center no-repeat; background-size: cover">
    <div class="slider-parallax-inner">
        <div class="container-fluid vertical-middle clearfix">
            <div class="heading-block title-center nobottomborder">
                <h1 style="color:black;">Keki Helping</h1>
                <span style="color:black; font-size:16px;">Kami tim Kelola kisah, siap membantu kamu untuk bertumbuh dan berkembang mengelola kisahnya. Kisah apapun yang sedang dialami saat ini, bisa kamu ceritakan ke Psikolog Kelola Kisah. </span>
            </div>

            <div class="center bottommargin">
                <a href="#section-pricing" data-href="#section-pricing" class="button button-3d button-teal button-xlarge nobottommargin"><i class="icon-star3"></i>Daftar Konsultasi</a>
            </div>
        </div>
    </div>
</section>
<!-- <section id="slider" class="slider-element slider-parallax swiper_wrapper clearfix">

    <div class="slider-parallax-inner">

        <div class="swiper-container swiper-parent">
            <div class="swiper-wrapper">
                <div class="swiper-slide" style="background-image: url('<?= base_url(); ?>assets/media/slideshow/banner2.png');">
                    <div class="container clearfix">
                        <div class="slider-caption slider-caption-center">
                            <h2 data-animate="fadeInUp">Konsultasi dengan Psikolog Keki</h2>
                            <p class="d-none d-sm-block" data-animate="fadeInUp" data-delay="200">Kamu merasa mulai hilang kendali akan hidupmu? Mungkin kamu merasa masalahmu mulai mengganggu banyak aspek kehidupanmu. Atau bahkan, apa mungkin masalahmu membuatmu membahayakan diri sendiri atau orang lain? Konsultasikan Aja Dengan Psikolog Kelola Kisah!</p>
                        </div>
                    </div>
                </div>


            </div>

        </div>

    </div>

</section> -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <div class="col_two_third mb-2">

                <div style="position: relative;" data-height-xl="535" data-height-lg="442" data-height-md="338" data-height-sm="316" data-height-xs="201">
                    <img data-animate="fadeInLeft" src="<?= base_url(); ?>assets/media/slideshow/duduk.png" alt="Mac" style="position: absolute; top: 0; left: 0;">
                    <!-- <img data-animate="fadeInRight" data-delay="300" src="<?= base_url(); ?>assets/media/slideshow/keki.png" alt="iPad" style="position: absolute; top: 0; left: 0;"> -->
                    <!-- <img data-animate="fadeInUp" data-delay="600" src="<?= base_url(); ?>assets/front/images/landing/device3.png" alt="iPhone" style="position: absolute; top: 0; left: 0;"> -->
                </div>

            </div>

            <div class="col_one_third topmargin nobottommargin col_last" style="padding-top:20px;">

                <h3>Apa itu Keki Helping?</h3>

                <p>Keki helping adalah layanan konsultasi/konseling / konsultasi bersama psikolog Kelola kisah untuk memberikan bantuan dalam menghadai permasalahan yang sedang dialami.</p>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum, ab incidunt temporibus rerum odio accusantium.</p>

                <div class="divider divider-short"><i class="icon-circle"></i></div>
                <h3>Bagaimana cara berkonsultasi dengan psikolog Kelola kisah?</h3>
                <ul class="iconlist iconlist-large iconlist-color">
                    <li><i class="icon-ok-sign"></i> Daftar Konsultasi</li>
                    <li><i class="icon-ok-sign"></i> Anda akan dihubungi admin Kelola kisah</li>
                    <li><i class="icon-ok-sign"></i> Menentukan jadwal konseling / konsultasi</li>
                    <li><i class="icon-ok-sign"></i> Selesaikan pembayaran</li>
                    <li><i class="icon-ok-sign"></i> Mulai konsultasi :)</li>
                </ul>

            </div>


            <div class="divider divider-short divider-center"><i class="icon-circle"></i></div>

            <div id="section-features" class="heading-block title-center page-section">
                <h2>Kamu perlu menghubungi Keki Helping, ketika...</h2>
                <span>Banyak hal yang dapat Kamu konsultasikan di Keki Helping</span>
            </div>

            <div class="col_one_third">
                <div class="feature-box fbox-plain">
                    <div class="fbox-icon" data-animate="bounceIn">
                        <a href="#"><img src="<?= base_url(); ?>assets/front/images/icons/features/responsive.png" alt="Responsive Layout"></a>
                    </div>

                    <p>Kamu memiliki masalah yang telah mengganggu aktivitas harianmu</p>
                </div>
            </div>

            <div class="col_one_third">
                <div class="feature-box fbox-plain">
                    <div class="fbox-icon" data-animate="bounceIn" data-delay="200">
                        <a href="#"><img src="<?= base_url(); ?>assets/front/images/icons/features/retina.png" alt="Retina Graphics"></a>
                    </div>

                    <p>Pernah atau baru saja mengalami kejadian traumatis</p>
                </div>
            </div>

            <div class="col_one_third col_last">
                <div class="feature-box fbox-plain">
                    <div class="fbox-icon" data-animate="bounceIn" data-delay="400">
                        <a href="#"><img src="<?= base_url(); ?>assets/front/images/icons/features/performance.png" alt="Powerful Performance"></a>
                    </div>
                    <p>Kamu memiliki perasaan yang sulit dikontrol dan terlalu berlebihan</p>
                </div>
            </div>

            <div class="clear"></div>

            <div class="col_one_third">
                <div class="feature-box fbox-plain">
                    <div class="fbox-icon" data-animate="bounceIn" data-delay="600">
                        <a href="#"><img src="<?= base_url(); ?>assets/front/images/icons/features/flag.png" alt="Responsive Layout"></a>
                    </div>

                    <p>Perilakumu sudah mulai membahayakan diri sendiri maupu orang lain</p>
                </div>
            </div>

            <div class="col_one_third">
                <div class="feature-box fbox-plain">
                    <div class="fbox-icon" data-animate="bounceIn" data-delay="800">
                        <a href="#"><img src="<?= base_url(); ?>assets/front/images/icons/features/tick.png" alt="Retina Graphics"></a>
                    </div>

                    <p>Orang disektiarmu mulai khawatir dan/atau menilai bahwa kamu sedang dalam kondisi tidak biasa</p>
                </div>
            </div>



            <div class="clear"></div>

            <div class="divider divider-short divider-center"><i class="icon-circle"></i></div>

            <div id="section-pricing" class="heading-block title-center page-section">
                <h2>Layanan Konseling / Konsultasi bersama psikolog Kelola kisah</h2>
                <span>Dapatkan layanan konsultasi sesuai dengan kebutuhanmu.</span>
            </div>

            <div class="pricing-box pricing-extended bottommargin clearfix">

                <div class="pricing-desc">
                    <div class="pricing-title">
                        <h3>Konsultasi / Konseling Psikologi mengenai:</h3>
                    </div>
                    <div class="pricing-features">
                        <ul class="clearfix">
                            <li><i class="icon-check"></i> Perkembangan anak & remaja</li>
                            <li><i class="icon-check"></i> Kesiapan masuk sekolah dasar anak, minat & bakat</li>
                            <li><i class="icon-check"></i> Sosioemosi anak, remaja, & dewasa</li>
                            <li><i class="icon-check"></i> Relasi orangtua & anak</li>
                            <li><i class="icon-check"></i> Permasalahan dewasa pasa berbagai setting kehidupan</li>
                            <li><i class="icon-check"></i> Romantic relationship, pranikah, marriage problem dll</li>
                            <li><i class="icon-check"></i> Pengembangan karir</li>
                            <li><i class="icon-support"></i> Platform : Zoom Meeting & Google Meet</li>
                        </ul>
                    </div>
                </div>

                <div class="pricing-action-area">
                    <div class="pricing-meta">
                        Price
                    </div>
                    <div class="pricing-price">
                        <span class="price-tenure"><del>Rp. 150.000</del></sup></span>
                        <span class="price-unit">120.000</span>
                    </div>
                    <div class="pricing-action">
                        <a href="https://api.whatsapp.com/send?phone=6289670097580&text=Halo%20Saya%20Ingin%20Mendaftar%20Konsultasi%20Keki" class="button button-3d button-xlarge btn-block nomargin">Daftar</a>
                    </div>
                </div>

            </div>

            <div class="divider divider-short divider-center"><i class="icon-circle"></i></div>

            <div id="section-faqs" class="heading-block title-center page-section">
                <h2>Paling Sering Ditanyakan</h2>
                <span>Kami telah menjawab berbagai Pertanyaan untuk Kenyamanan Anda</span>
            </div>

            <div id="faqs" class="faqs">

                <div class="toggle faq faq-marketplace faq-authors">
                    <div class="togglet"><i class="toggle-closed icon-question-sign"></i><i class="toggle-open icon-question-sign"></i>Bagaimana cara mendaftar Konseling / Konsultasi Keki? Berapa harganya?</div>
                    <div class="togglec">Kamu bisa langsung menekan tombol daftar dan kemudian akan dihubungkan langsung ke admin Keki ya...</div>
                </div>

                <div class="toggle faq faq-authors faq-miscellaneous">
                    <div class="togglet"><i class="toggle-closed icon-comments-alt"></i><i class="toggle-open icon-comments-alt"></i>Kapan saja jam operasional admin Keki?</div>
                    <div class="togglec">Jam kerja admin:
                        <br>
                        Setiap hari (Senin-Minggu)
                        <br>
                        Jam 09:00 - 19:00 WIB
                    </div>
                </div>

                <div class="toggle faq faq-miscellaneous">
                    <div class="togglet"><i class="toggle-closed icon-lock3"></i><i class="toggle-open icon-lock3"></i>Platform apa yang digunakan untuk konseling / konsultasi?</div>
                    <div class="togglec">Platform yang digunakan adalah Google Meet</div>
                </div>

                <div class="toggle faq faq-authors faq-legal faq-itemdiscussion">
                    <div class="togglet"><i class="toggle-closed icon-download-alt"></i><i class="toggle-open icon-download-alt"></i>Apakah tersedia layanan Konseling / Konsultasi tatap muka?</div>
                    <div class="togglec">Mohon maaf, metode tatap muka saat ini tidak tersedia hingga waktu yang belum ditentukan akibat adanya pandemi COVID.</div>
                </div>

            </div>

            <div class="clear"></div>

            <div class="divider divider-short divider-center"><i class="icon-circle"></i></div>

            <div id="section-contact" class="heading-block title-center page-section">
                <h2>Hubungi Kami</h2>
                <span>Ada pertanyaan lain? Hubungi kami melalui form di bawah</span>
            </div>

            <!-- Contact Form
					============================================= -->


            <!-- Contact Info
					============================================= -->
            <div class="col_full nobottommargin clearfix">

                <div class="col_one_fourth">
                    <div class="feature-box fbox-center fbox-bg fbox-plain">
                        <div class="fbox-icon">
                            <a href="https://www.instagram.com/kelolakisah/"><i class="icon-instagram"></i></a>
                        </div>
                        <h3>Instagram kami<span class="subtitle">@kelolakisah</span></h3>
                    </div>
                </div>

                <div class="col_one_fourth">
                    <div class="feature-box fbox-center fbox-bg fbox-plain">
                        <div class="fbox-icon">
                            <a href="https://api.whatsapp.com/send?phone=6289670097580&text=Halo%20Saya%20Ingin%20Mendaftar%20Konsultasi%20Keki"><i class="icon-whatsapp"></i></a>
                        </div>
                        <h3>WhatsApp<span class="subtitle">+62 896-7009-7580</span></h3>
                    </div>
                </div>

                <div class="col_one_fourth">
                    <div class="feature-box fbox-center fbox-bg fbox-plain">
                        <div class="fbox-icon">
                            <a href="https://www.youtube.com/channel/UCvdyjWQ94VkqkTHpY0s7eOQ"><i class="icon-youtube"></i></a>
                        </div>
                        <h3>YouTube<span class="subtitle">Kelola Kisah</span></h3>
                    </div>
                </div>

                <div class="col_one_fourth col_last">
                    <div class="feature-box fbox-center fbox-bg fbox-plain">
                        <div class="fbox-icon">
                            <a href="mailto:kelolakisah@gmail.com"><i class="icon-email"></i></a>
                        </div>
                        <h3>Email<span class="subtitle">kelolakisah@gmail.com</span></h3>
                    </div>
                </div>

            </div><!-- Contact Info End -->

        </div>

    </div>

</section><!-- #content end -->
<?php include("footer-front.php"); ?>