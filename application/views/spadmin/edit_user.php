<?php include('header-adm.php'); ?>
<?php include('sidebar-adm.php'); ?>
<?php include('navigation-adm.php'); ?>

<?php

$row = $users;
?>
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-1 text-gray-800">Ubah Informasi Admin Keki</h1>
    <br>

    <!-- Content Row -->
    <div class="row">

        <!-- Grow In Utility -->
        <div class="col-lg-6">

            <div class="card position-relative">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Form edit informasi admin</h6>
                </div>
                <div class="card-body">
                    <div class="mb-3">
                        <?php
                        if ($this->session->flashdata('error') != '') {
                            echo '<div class="alert alert-danger" role="alert">';
                            echo $this->session->flashdata('error');
                            echo '</div>';
                        }
                        if ($this->session->flashdata('success') != '') {
                            echo '<div class="alert alert-success" role="alert">';
                            echo $this->session->flashdata('success');
                            echo '</div>';
                        }
                        ?>
                        <form method="post" action="<?= base_url(); ?>spadmin/manage_user/update/<?= $row->user_id ?>">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Username</label>
                                <input type="text" value="<?= $row->username; ?>" class="form-control" disabled>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nama Admin</label>
                                <input type="text" name="nama" value="<?= $row->nama; ?>" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Password</label>
                                <input type="password" name="password" class="form-control" required>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>

                </div>
            </div>

        </div>

        <!-- Fade In Utility -->
        <div class="col-lg-6">

            <div class="card position-relative">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Ketentuan dalam perubahan data Admin</h6>
                </div>
                <div class="card-body">
                    <div class="mb-3">
                        <ul>
                            <li><code>Pastikan password yang digunakan tidak mudah di acak oleh robot, gunakan simbol (direkomendasikan)</code></li>
                            <li><code>Jangan melakukan edit password dengan username <b>'root'</b></code></li>
                        </ul>

                    </div>
                    <p class="mb-0 small">Note: This utility animates the CSS opacity property, meaning
                        it will override any existing opacity on an element being animated!</p>
                </div>
            </div>

        </div>

    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
<?php include('footer-adm.php'); ?>