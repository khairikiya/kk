<?php include('header-adm.php'); ?>
<?php include('sidebar-adm.php'); ?>
<?php include('navigation-adm.php'); ?>
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Daftar Slideshow Gambar</h1>
    <br>

    <p class="mb-4"><a href="#" data-toggle="modal" data-target="#addSlideshowModal" class="btn btn-primary btn-icon-split">
            <span class="icon text-white-50">
                <i class="fas fa-plus"></i>
            </span>
            <span class="text">Tambah Slideshow</span>
        </a></p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Daftar Slideshow (HOME)</h6>
        </div>
        <div class="card-body">
            <?php
            if ($this->session->flashdata('error') != '') {
                echo '<div class="alert alert-danger" role="alert">';
                echo $this->session->flashdata('error');
                echo '</div>';
            }
            if ($this->session->flashdata('success') != '') {
                echo '<div class="alert alert-success" role="alert">';
                echo $this->session->flashdata('success');
                echo '</div>';
            }
            ?>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Judul</th>
                            <th>Deskripsi</th>
                            <th>Urutan</th>
                            <th>Action</th>

                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($slide as $row) :
                        ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><?= $row->judul_slideshow ?></td>
                                <td><?= $row->deskripsi_slideshow ?></td>
                                <td><?= $row->urutan ?></td>
                                <td><a class="btn btn-warning" href="<?= base_url(); ?>spadmin/manage_slideshow/delete/<?= $row->slideshow_id ?>"><i class="fa fa-trash"></i></a></td>
                            </tr>
                        <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<div class="modal fade" id="addSlideshowModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Gambar Slideshow</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url(); ?>spadmin/manage_slideshow/add" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Judul Slideshow:</label>
                        <input type="text" name="judul_slideshow" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Deskripsi Slideshow:</label>
                        <input type="text" name="deskripsi_slideshow" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Gambar Slide:</label>
                        <input type="file" name="media" class="form-control-file">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Urutan Slideshow:</label>
                        <select class="form-control" name="urutan" id="">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
<?php include('footer-adm.php'); ?>