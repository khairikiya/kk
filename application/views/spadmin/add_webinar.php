<?php include('header-adm.php'); ?>
<?php include('sidebar-adm.php'); ?>
<?php include('navigation-adm.php'); ?>
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Add Webinar</h1>
    <br>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Form Tambah Webinar</h6>
        </div>
        <div class="card-body">
            <?php
            if ($this->session->flashdata('error') != '') {
                echo '<div class="alert alert-danger" role="alert">';
                echo $this->session->flashdata('error');
                echo '</div>';
            }
            if ($this->session->flashdata('success') != '') {
                echo '<div class="alert alert-success" role="alert">';
                echo $this->session->flashdata('success');
                echo '</div>';
            }
            ?>

            <form method="POST" action="<?= base_url(); ?>spadmin/manage_webinar/add" enctype="multipart/form-data">
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Judul Webinar</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputPassword" name="judul_webinar">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Tanggal Webinar</label>
                    <div class="col-sm-5">
                        <input type="datetime-local" class="form-control" id="inputPassword" name="tgl_webinar">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Nama Psikolog</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="nama_psikolog" name="nama_psikolog">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Banner (Gambar Thumb) Webinar</label>
                    <div class="col-sm-5">
                        <input type="file" class="form-control-file" id="media" name="media">
                    </div>
                </div>
                <!-- CKEDITOR SCRIPT -->

                <script src="<?= base_url(); ?>assets/ckeditor/ckeditor.js"></script>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Isi Webinar</label>
                    <div class="col-sm-10">
                        <textarea type="datetime-local" class="form-control" id="isi_webinar" name="isi_webinar"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Webinar Status</label>
                    <div class="col-sm-4">
                        <select class="form-control" name="post_status" id="post_status">
                            <option value="Publish">Publish</option>
                            <option value="Draft">Draft</option>
                        </select>
                    </div>
                </div>
                <br>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label"></label>
                    <div class="col-sm-2 mb-2">
                        <button type="submit" class="btn btn-success col-sm-12"><i class="fa fa-check"></i> Save</button>
                    </div>
                    <div class="col-sm-2 mb-2">
                        <a href="<?= base_url(); ?>spadmin/manage_webinar" class="btn btn-danger col-sm-12"><i class="fa fa-window-close"></i> Cancel</a>
                    </div>
                </div>

                <script>
                    // Replace the <textarea id="editor1"> with a CKEditor 4
                    // instance, using default configuration.
                    CKEDITOR.replace('isi_webinar');
                </script>
            </form>

        </div>
    </div>

</div>


</div>
<!-- End of Main Content -->
<?php include('footer-adm.php'); ?>