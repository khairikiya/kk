		<!-- Footer
		============================================= -->
		<footer id="footer" class="dark">


			<!-- Copyrights
			============================================= -->
			<div id="copyrights">

				<div class="container clearfix">

					<div class="col_half">
						Copyrights &copy; 2021 All Rights Reserved by Kelola Kisah.<br>
					</div>

					<div class="col_half col_last tright">


						<div class="clear"></div>

						<i class="icon-envelope2"></i> kelolakisah@gmail.com <span class="middot">&middot;</span> <i class="icon-headphones"></i> +62 896-7009-7580
					</div>

				</div>

			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->

		</div><!-- #wrapper end -->

		<!-- Go To Top
	============================================= -->
		<div id="gotoTop" class="icon-angle-up"></div>

		<!-- External JavaScripts
	============================================= -->
		<script src="<?= base_url(); ?>assets/front/js/jquery.js"></script>
		<script src="<?= base_url(); ?>assets/front/js/plugins.js"></script>

		<!-- Footer Scripts
	============================================= -->
		<script src="<?= base_url(); ?>assets/front/js/functions.js"></script>


		<script src="<?= base_url(); ?>assets/front/js/jquery.gmap.js"></script>



		</body>

		</html>