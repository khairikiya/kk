<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets
	============================================= -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/front/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/front/style.css" type="text/css" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/front/css/dark.css" type="text/css" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/front/css/swiper.css" type="text/css" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/front/css/font-icons.css" type="text/css" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/front/css/animate.css" type="text/css" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/front/css/magnific-popup.css" type="text/css" />
    <link rel="shortcut icon" height="100%" type="image/png" href="https://i.ibb.co/wwbGhpQ/FIX-LOGO-removebg-preview-removebg-preview.png" />
    <link rel="stylesheet" href="<?= base_url(); ?>assets/front/css/responsive.css" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Document Title
	============================================= -->
    <title>Kelola Kisah | Konsultasi / Konseling</title>

</head>