<body class="stretched">

    <!-- Document Wrapper
	============================================= -->
    <div id="wrapper" class="clearfix">

        <!-- Header
		============================================= -->
        <header id="header" class="page-section">

            <div id="header-wrap">

                <div class="container clearfix">

                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                    <!-- Logo
					============================================= -->
                    <div id="logo">
                        <a href="index.html" class="standard-logo" data-dark-logo="<?= base_url(); ?>assets/front/images/logo-dark.png"><img src="<?= base_url(); ?>assets/media/logo/logo.png" alt="Canvas Logo"></a>
                        <a href="index.html" class="retina-logo" data-dark-logo="<?= base_url(); ?>assets/front/images/logo-dark@2x.png"><img src="<?= base_url(); ?>assets/media/logo/logo.png" alt="Canvas Logo"></a>
                    </div><!-- #logo end -->

                    <!-- Primary Navigation
					============================================= -->
                    <nav id="primary-menu">

                        <ul class="one-page-menu">
                            <!-- <li class="current"><a href="#" data-href="#section-home">
                                    <div>Beranda</div>
                                </a></li> -->
                            <li><a href="#" data-href="#section-features">
                                    <div>Layanan</div>
                                </a></li>
                            <li><a href="#" data-href="#section-pricing">
                                    <div>Konsultasi</div>
                                </a></li>
                            <li><a href="#" data-href="#section-faqs">
                                    <div>FAQs</div>
                                </a></li>
                            <li><a href="#" data-href="#section-contact">
                                    <div>Kontak</div>
                                </a></li>
                            <li><a href="https://www.instagram.com/kelolakisah/">
                                    <div>Tentang Keki</div>
                                </a></li>
                        </ul>

                    </nav><!-- #primary-menu end -->

                </div>

            </div>

        </header><!-- #header end -->