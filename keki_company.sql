/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100419
 Source Host           : localhost:3306
 Source Schema         : keki_company

 Target Server Type    : MySQL
 Target Server Version : 100419
 File Encoding         : 65001

 Date: 15/08/2021 13:07:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for halaman
-- ----------------------------
DROP TABLE IF EXISTS `halaman`;
CREATE TABLE `halaman`  (
  `halaman_id` int NOT NULL,
  `judul_halaman` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `isi_halaman` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jenis_halaman` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `user_id` int NULL DEFAULT NULL,
  `status` int NULL DEFAULT 1,
  PRIMARY KEY (`halaman_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of halaman
-- ----------------------------

-- ----------------------------
-- Table structure for slideshow
-- ----------------------------
DROP TABLE IF EXISTS `slideshow`;
CREATE TABLE `slideshow`  (
  `slideshow_id` int NOT NULL AUTO_INCREMENT,
  `judul_slideshow` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deskripsi_slideshow` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `img_slideshow` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `urutan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` int NULL DEFAULT 1,
  PRIMARY KEY (`slideshow_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of slideshow
-- ----------------------------
INSERT INTO `slideshow` VALUES (3, 'Slideshow 1', 'Deskripsi Slideshow 1', 'Slideshow_120210727084916.png', '1', 1);
INSERT INTO `slideshow` VALUES (4, 'Slideshow 2', 'Deskripsi Slideshow 2', 'Slideshow_220210727092248.png', '2', 2);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` int NULL DEFAULT 1,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Root Account', 'root', '$2y$10$agTiB.yWnbDln1KjNlNuMuyXmqiYyp9LMPH.8VOJ2fQ6V/ppJucDm', 1);
INSERT INTO `users` VALUES (2, 'Khairi', 'khairi', '$2y$10$Zpo6.DzawSUzQ8bqiMAGtevkBMWel6ZP8lgp7c/xlZ2NM7UQrnAyW', 1);

-- ----------------------------
-- Table structure for webinar
-- ----------------------------
DROP TABLE IF EXISTS `webinar`;
CREATE TABLE `webinar`  (
  `webinar_id` int NOT NULL AUTO_INCREMENT,
  `judul_webinar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `isi_webinar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tgl_webinar` datetime NULL DEFAULT NULL,
  `nama_psikolog` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `img_webinar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `user_id` int NULL DEFAULT NULL,
  `post_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` int NULL DEFAULT 1,
  PRIMARY KEY (`webinar_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of webinar
-- ----------------------------
INSERT INTO `webinar` VALUES (1, 'Kelola Kisah Webinar Test 11', '<p>Ini adalah isi Webinar yang dapat dibuat semenarik mungkin</p>\r\n', '0000-00-00 00:00:00', 'Mrs. Refalio', 'img.jpg', NULL, 'Draft', 1);
INSERT INTO `webinar` VALUES (2, 'JUDUL', '<p>ABCD</p>\r\n', '2021-07-28 00:00:00', 'ABCD', 'JUDUL20210728092224.png', NULL, 'Publish', 1);
INSERT INTO `webinar` VALUES (3, NULL, NULL, '2021-07-28 00:00:00', NULL, NULL, NULL, 'Publish', 1);
INSERT INTO `webinar` VALUES (4, 'ABCDD', '<p>AS</p>\r\n', '2021-07-28 00:00:00', 'EKA', 'ABCDD20210728092604.png', NULL, 'Draft', 1);
INSERT INTO `webinar` VALUES (5, 'AD', '<p>AS</p>\r\n', '2021-07-16 17:30:00', 'S', 'AD20210728092724.png', NULL, 'Publish', 2);
INSERT INTO `webinar` VALUES (6, 'Kelola Kisah Mental Health', '<p>ACC</p>\r\n', '2021-07-02 16:43:00', 'ABCD', 'Kelola_Kisah_Mental_Health20210728094400.png', NULL, NULL, 2);

SET FOREIGN_KEY_CHECKS = 1;
